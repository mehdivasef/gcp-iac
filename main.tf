# This is a single-line comment.

// This is also a single-line comment.

/*
This is a multi-line comment.
*/

resource "google_storage_bucket" "auto-expire" {
  name          = "cloudquicklabs_gcp_bucket_iac-22"
  location      = "US"
  force_destroy = true

  public_access_prevention = "enforced"
}