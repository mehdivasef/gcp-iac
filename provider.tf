provider "google" {
   credentials = "${file("./creds/serviceaccount.json")}"
   project     = "resonant-triode-408814" # REPLACE WITH YOUR PROJECT ID
   region      = "US"
 }